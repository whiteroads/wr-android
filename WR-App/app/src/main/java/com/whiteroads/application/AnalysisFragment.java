package com.whiteroads.application;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.whiteroads.application.data.UserDataWrapper;
import com.whiteroads.application.utils.CircleTransform;
import com.whiteroads.application.utils.CommonMethods;
import com.whiteroads.library.StartService;
import com.whiteroads.library.interfaces.DataEventListener;

import java.util.ArrayList;
import java.util.Random;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.SENSOR_SERVICE;

public class AnalysisFragment extends Fragment implements DataEventListener, LocationListener,SensorEventListener {
    private TextView address, score, name, email, mobile, vehicle, steps, apVersion, trips, kms,quote;
    private BroadcastReceiver broadcastReceiver;
    private ImageView imageUser;
    private RelativeLayout profileLayout;
    private LocationManager locationManager;
    private SensorManager sensorManager;
    private Sensor stepSensor;
    private ArrayList<String> quotes;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view = inflater.inflate(R.layout.fragment_analysis, container, false);
        try {
            score = (TextView) view.findViewById(R.id.score);
            address = (TextView) view.findViewById(R.id.location);
            name = (TextView) view.findViewById(R.id.userName);
            email = (TextView) view.findViewById(R.id.email);
            mobile = (TextView) view.findViewById(R.id.mobile);
            vehicle = (TextView) view.findViewById(R.id.vehicle);
            steps = (TextView) view.findViewById(R.id.steps);
            trips = (TextView) view.findViewById(R.id.trips);
            kms = (TextView) view.findViewById(R.id.kms);
            apVersion = (TextView) view.findViewById(R.id.apVersion);
            quote = (TextView) view.findViewById(R.id.quote);
            imageUser = (ImageView) view.findViewById(R.id.imageUser);
            profileLayout = (RelativeLayout) view.findViewById(R.id.profileLayout);
            profileLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showProfileDialog();
                }
            });
            quotes = new ArrayList<>();
            quotes.add("When turning your steering wheel, don’t hold it in an extreme right or left position for more than a few seconds");
            quotes.add("Avoid driving at high speeds and accelerating quickly, especially when it’s very hot or very cold outside");
            quotes.add("Don't use mobile phone or any other gadgets while driving");
            quotes.add("Don't drink and drive!");
            quotes.add("Don't go fast on Green lights");
            quotes.add("Try not be used to with a single vehicle. Try different make and model whenever you get a chance");

            Random r = new Random();
            int i = r.nextInt(quotes.size() - 0) + 0;

            quote.setText(quotes.get(i));

            apVersion.setText("App Version: " + BuildConfig.VERSION_NAME);
            email.setText(Html.fromHtml("Email: <b>" + UserDataWrapper.getInstance().getUserEmail() + "</b>"));
            mobile.setText(Html.fromHtml("Mob: <b>" + UserDataWrapper.getInstance().getUserMobile() + "</b>"));
            vehicle.setText(Html.fromHtml("Vehicle No: <b>" + UserDataWrapper.getInstance().getVehicleNumber() + "</b>"));
            name.setText(Html.fromHtml(UserDataWrapper.getInstance().getUserName()));
            kms.setText(Html.fromHtml("<b>" + (int) UserDataWrapper.getInstance().getTotalDistance() + "</b><br><small>Kms</small>"));
            trips.setText(Html.fromHtml("<b>" + UserDataWrapper.getInstance().getTotalTrips() + "</b><br><small>Trips</small>"));
            Picasso.with(getActivity()).load(UserDataWrapper.getInstance().getUserPic())
                    .placeholder(R.mipmap.img_person)
                    .error(R.mipmap.img_person)
                    .transform(new CircleTransform())
                    .resize(getResources().getDrawable(R.mipmap.img_person).getIntrinsicWidth() - 32,
                            getResources().getDrawable(R.mipmap.img_person).getIntrinsicHeight() - 32).into(imageUser);
            StartService startService = new StartService(getActivity());
            startService.registerForDataUpdates(this);
            score.setText("Tips");
            trips.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((HomeActivity) getActivity()).changeToTrip();
                }
            });

            locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            if(ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 100, this);
            }

            sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);
            if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) != null
                    && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
                sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_STATUS_ACCURACY_HIGH);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void updateData(){
        kms.setText(Html.fromHtml("<b>"+(int)UserDataWrapper.getInstance().getTotalDistance()+"</b><br><small>Kms</small>"));
        trips.setText(Html.fromHtml("<b>"+UserDataWrapper.getInstance().getTotalTrips()+"</b><br><small>Trips</small>"));
    }

    @Override
    public void onAccelChange(String value) {

    }

    @Override
    public void onLinAccelChange(String value) {

    }

    @Override
    public void onSpeedChange(String value) {

    }

    @Override
    public void onLatLonChange(String value) {

    }

    @Override
    public void onGyroChange(String value) {

    }

    @Override
    public void onStepsChange(String value) {
        steps.setText(Html.fromHtml("<b>"+value+"</b><br><small>Steps</small>"));
    }

    @Override
    public void onProxiChange(String value) {

    }

    @Override
    public void onRotationChange(String value) {

    }

    @Override
    public void onOrientChange(String value) {

    }

    @Override
    public void onLightChange(String value) {

    }

    @Override
    public void onScoreChange(int value) {
    }

    @Override
    public void onAddressChange(String value) {
        address.setText(value);
    }

    public void showProfileDialog() {
        try {
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.profile_dialog);

            TextView name = (TextView) dialog.findViewById(R.id.userName);
            TextView email = (TextView) dialog.findViewById(R.id.email);
            TextView mobile = (TextView) dialog.findViewById(R.id.mobile);
            TextView vehicle = (TextView) dialog.findViewById(R.id.vehicle);
            ImageView imageUser = (ImageView) dialog.findViewById(R.id.imageUser);
            email.setText(Html.fromHtml("Email: <b>"+UserDataWrapper.getInstance().getUserEmail()+"</b>"));
            mobile.setText(Html.fromHtml("Mob: <b>"+UserDataWrapper.getInstance().getUserMobile()+"</b>"));
            vehicle.setText(Html.fromHtml("Vehicle No: <b>"+UserDataWrapper.getInstance().getVehicleNumber()+"</b>"));
            name.setText(Html.fromHtml(UserDataWrapper.getInstance().getUserName()));
            Picasso.with(getActivity()).load(UserDataWrapper.getInstance().getUserPic())
                    .placeholder(R.mipmap.score_bg)
                    .transform(new CircleTransform())
                    .resize(getResources().getDrawable(R.mipmap.score_bg).getIntrinsicWidth()-32,
                            getResources().getDrawable(R.mipmap.score_bg).getIntrinsicHeight()-32).into(imageUser);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        address.setText(CommonMethods.getAddressComplete(getActivity(),location));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            steps.setText(Html.fromHtml(String.valueOf(String.valueOf(sensorEvent.values[0]))+"<br>Steps"));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
