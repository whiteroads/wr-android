package com.whiteroads.application;

import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.whiteroads.application.model.TripItem;
import com.whiteroads.application.model.TripSingleEntry;
import com.whiteroads.application.utils.CommonMethods;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends FragmentActivity {


    private MapView mapView;
    private ScrollView scrollView;
    private TextView rapidCount, speedCount,phoneCount,breaksCount,tripTime,distance,roadsDamage,rating,roadsDamagePercent;
    private TripItem tripItem;
    private List<Point> routeCoordinates;
    private LatLng locationStart;
    private LatLng locationEnd;
    private List<LatLng> points;
    private Marker marker;
    private LinearLayout rapidLyt,osLyt,breakLyt,phoneLyt,roadLyt;
    private MapboxMap mapboxMap;
    private List<Marker> rapidMarkers = new ArrayList<>();
    private List<Marker> osMarkers = new ArrayList<>();
    private List<Marker> breakMarkers = new ArrayList<>();
    private List<Marker> phoneMarkers = new ArrayList<>();
    private List<Marker> roadMarkers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Mapbox.getInstance(this, "pk.eyJ1Ijoid2hpdGVyb2FkcyIsImEiOiJjanB6bGJmM3AwYzJnM3hxajZoZHZuZmgxIn0.QP_yzqDa3feWJr1boDmAGw");
        setContentView(R.layout.activity_trip_detail);
        try {
            mapView = (MapView) findViewById(R.id.mapView);
            scrollView = (ScrollView) findViewById(R.id.scrollView);
            rapidCount = (TextView) findViewById(R.id.rapidCount);
            phoneCount = (TextView) findViewById(R.id.phoneCount);
            speedCount = (TextView) findViewById(R.id.speedCount);
            breaksCount = (TextView) findViewById(R.id.breaksCount);
            tripTime = (TextView) findViewById(R.id.tripTime);
            distance = (TextView) findViewById(R.id.distance);
            roadsDamage = (TextView) findViewById(R.id.roadsDamage);
            rating = (TextView) findViewById(R.id.rating);
            roadsDamagePercent = (TextView) findViewById(R.id.roadsDamagePercent);
            rapidLyt = (LinearLayout) findViewById(R.id.accelLyt);
            osLyt = (LinearLayout) findViewById(R.id.osLyt);
            breakLyt = (LinearLayout) findViewById(R.id.breaksLyt);
            phoneLyt = (LinearLayout) findViewById(R.id.phoneLyt);
            roadLyt = (LinearLayout) findViewById(R.id.roadsLyt);
            mapView.onCreate(savedInstanceState);
            tripItem = TripSingleton.getObject().getTripItem();
            if(tripItem!=null){
                rapidCount.setText(tripItem.getRapidAccelaration()+"");
                phoneCount.setText(tripItem.getPhoneUsesMinutes());
                breaksCount.setText(tripItem.getHardbreaking()+"");
                speedCount.setText(tripItem.getOverspeedCount()+"");

                tripTime.setText(CommonMethods.GetTwoDatesTimeDifferenceMinutes(tripItem.getEntries().get(0).getPingTime(),
                        tripItem.getEntries().get(tripItem.getEntries().size()-1).getPingTime())+" Minutes");
                distance.setText(CommonMethods.round(tripItem.getDistance()/1000)+" Kms");
            }

            routeCoordinates = new ArrayList<Point>();
            roadsDamage.setText(tripItem.getRoadDamage()+"%");
            roadsDamagePercent.setText(tripItem.getRoadDamage()+"%");
            rating.setText(tripItem.getRating());
            points = new ArrayList<>();
            for(TripSingleEntry entry: tripItem.getEntries()) {
                routeCoordinates.add(Point.fromLngLat(Double.parseDouble(entry.getLng()), Double.parseDouble(entry.getLat())));
                points.add(new LatLng(Double.parseDouble(entry.getLat()), Double.parseDouble(entry.getLng())));
            }

            rapidLyt.setTag(0);
            osLyt.setTag(0);
            breakLyt.setTag(0);
            phoneLyt.setTag(0);
            roadLyt.setTag(0);

            rapidLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(rapidLyt.getTag().toString())==0){
                        IconFactory iconFactory = IconFactory.getInstance(DetailActivity.this);
                        Icon icon = iconFactory.fromResource(R.mipmap.rapid_white_small);
                        for(TripSingleEntry item : tripItem.getEntries()){
                            if(item.getAcceleration()==1) {
                                rapidMarkers.add(mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng()))))
                                        .icon(icon)
                                        .title(getString(R.string.accel))));
                            }
                        }
                        rapidLyt.setBackgroundResource(R.mipmap.rect_selected);
                        rapidLyt.setTag(1);
                    }else{
                        for(Marker marker : rapidMarkers){
                            mapboxMap.removeMarker(marker);
                        }
                        rapidLyt.setBackgroundResource(R.mipmap.rect_not_selected);
                        rapidLyt.setTag(0);
                    }
                }
            });
            osLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(osLyt.getTag().toString())==0){
                        IconFactory iconFactory = IconFactory.getInstance(DetailActivity.this);
                        Icon icon = iconFactory.fromResource(R.mipmap.overspeed_white_small);
                        for(TripSingleEntry item : tripItem.getEntries()){
                            if(item.getOverspeed()==1) {
                                osMarkers.add(mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng()))))
                                        .icon(icon)
                                        .title(getString(R.string.overspeed))));
                            }
                        }
                        osLyt.setBackgroundResource(R.mipmap.rect_selected);
                        osLyt.setTag(1);
                    }else{
                        for(Marker marker : osMarkers){
                            mapboxMap.removeMarker(marker);
                        }
                        osLyt.setBackgroundResource(R.mipmap.rect_not_selected);
                        osLyt.setTag(0);
                    }
                }
            });
            phoneLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(phoneLyt.getTag().toString())==0){
                        IconFactory iconFactory = IconFactory.getInstance(DetailActivity.this);
                        Icon icon = iconFactory.fromResource(R.mipmap.phone_white_small);
                        for(TripSingleEntry item : tripItem.getEntries()){
                            if(item.getCallState()!=null && !item.getCallState().equalsIgnoreCase("Idle")
                                    && !item.getCallState().equalsIgnoreCase("nan")) {
                                phoneMarkers.add(mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng()))))
                                        .icon(icon)
                                        .title(getString(R.string.phone_use))));
                            }
                        }
                        phoneLyt.setBackgroundResource(R.mipmap.rect_selected);
                        phoneLyt.setTag(1);
                    }else{
                        for(Marker marker : phoneMarkers){
                            mapboxMap.removeMarker(marker);
                        }
                        phoneLyt.setBackgroundResource(R.mipmap.rect_not_selected);
                        phoneLyt.setTag(0);
                    }
                }
            });
            breakLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(breakLyt.getTag().toString())==0){
                        IconFactory iconFactory = IconFactory.getInstance(DetailActivity.this);
                        Icon icon = iconFactory.fromResource(R.mipmap.hard_breaking_white_small);
                        for(TripSingleEntry item : tripItem.getEntries()){
                            if(item.getAcceleration()==-1) {
                                breakMarkers.add(mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng()))))
                                        .icon(icon)
                                        .title(getString(R.string.hard_breaking))));
                            }
                        }
                        breakLyt.setBackgroundResource(R.mipmap.rect_selected);
                        breakLyt.setTag(1);
                    }else{
                        for(Marker marker : breakMarkers){
                            mapboxMap.removeMarker(marker);
                        }
                        breakLyt.setBackgroundResource(R.mipmap.rect_not_selected);
                        breakLyt.setTag(0);
                    }
                }
            });
            roadLyt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(Integer.parseInt(roadLyt.getTag().toString())==0){
                        IconFactory iconFactory = IconFactory.getInstance(DetailActivity.this);
                        Icon icon = iconFactory.fromResource(R.mipmap.red_dot);
                        for(TripSingleEntry item : tripItem.getEntries()){
                            if(item.getRoadDamage()==1) {
                                roadMarkers.add(mapboxMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(new LatLng(Double.parseDouble(item.getLat()), Double.parseDouble(item.getLng()))))
                                        .icon(icon)
                                        .title(getString(R.string.damage_road))));
                            }
                        }
                        roadLyt.setBackgroundResource(R.mipmap.rect_selected);
                        roadLyt.setTag(1);
                    }else{
                        for(Marker marker : roadMarkers){
                            mapboxMap.removeMarker(marker);
                        }
                        roadLyt.setBackgroundResource(R.mipmap.rect_not_selected);
                        roadLyt.setTag(0);
                    }
                }
            });


            locationStart = new LatLng(Double.parseDouble(tripItem.getEntries().get(0).getLat()), Double.parseDouble(tripItem.getEntries().get(0).getLng()));

            locationEnd = new LatLng(Double.parseDouble(tripItem.getEntries().get(tripItem.getEntries().size()-1).getLat()), Double.parseDouble(tripItem.getEntries().get(tripItem.getEntries().size()-1).getLng()));

            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    DetailActivity.this.mapboxMap = mapboxMap;
                    mapboxMap.setStyleUrl("mapbox://styles/whiteroads/cjqkhfrnycu322rmw6oubja44");
                    marker = mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(tripItem.getEntries().get(0).getLat()),
                                    Double.parseDouble(tripItem.getEntries().get(0).getLng()))));

                    LatLngBounds latLngBounds = new LatLngBounds.Builder()
                            .include(locationStart) // Northeast
                            .include(locationEnd) // Southwest
                            .build();

                    mapboxMap.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 500);

                    // Draw polyline on map
                    mapboxMap.addPolyline(new PolylineOptions()
                            .addAll(points)
                            .color(Color.parseColor("#ffffff"))
                            .width(5));
                    int i=0;
                    for(TripSingleEntry entry: tripItem.getEntries()) {
                        if(i>0) {
                            ValueAnimator markerAnimator = ObjectAnimator.ofObject(marker, "position",
                                    new LatLngEvaluator(), marker.getPosition(), new LatLng(Double.parseDouble(entry.getLat()), Double.parseDouble(entry.getLng())));
                            marker.setPosition(new LatLng(Double.parseDouble(entry.getLat()), Double.parseDouble(entry.getLng())));
                            markerAnimator.setDuration(500);
                            markerAnimator.start();
                        }
                        i++;
                    }

                }
            });
            mapView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            scrollView.requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                        case MotionEvent.ACTION_CANCEL:
                            scrollView.requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    return mapView.onTouchEvent(event);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class LatLngEvaluator implements TypeEvaluator<LatLng> {
        private LatLng latLng = new LatLng();

        @Override
        public LatLng evaluate(float fraction, LatLng startValue, LatLng endValue) {
            latLng.setLatitude(startValue.getLatitude()
                    + ((endValue.getLatitude() - startValue.getLatitude()) * fraction));
            latLng.setLongitude(startValue.getLongitude()
                    + ((endValue.getLongitude() - startValue.getLongitude()) * fraction));
            return latLng;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
