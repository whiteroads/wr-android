package com.whiteroads.application;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.plattysoft.leonids.ParticleSystem;
import com.whiteroads.application.data.UserDataWrapper;
import com.whiteroads.library.StartService;

public class HomeActivity extends FragmentActivity {

    private TextView accel, speed, gyroscope, linaccel, location, rotation, light, proxim, steps, orientation;
    private BroadcastReceiver broadcastReceiver;
    private Button stopCapture;
    private Intent service, sensors;
    private PendingIntent pIntent;
    private AlarmManager alarm;
    private BottomNavigationView navigation;
    public StartService startService;
    private AnalysisFragment analysisFragment;
    private SensorsFragment sensorsFragment;
    private FragmentTransaction transaction;
    private Fragment active;
    private Button startTracking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_home);
        try {
//            Toolbar toolbar = findViewById(R.id.toolbar);
//            setSupportActionBar(toolbar);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setTitle("");
//            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onBackPressed();
//                }
//            });
            startService = new StartService(this);
            startTracking = (Button) findViewById(R.id.startTracking);
            if (UserDataWrapper.getInstance().IsServiceStarted()) {
                startTracking.setText("Stop Journey");
            } else {
                startTracking.setText("Start Journey");
            }
            startTracking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!UserDataWrapper.getInstance().IsServiceStarted()) {
                        startService.initializeSDK("whiteroads", "6ca383af9aa7d7c6538e746927ddc829",
                                UserDataWrapper.getInstance().getUserEmail(),
                                UserDataWrapper.getInstance().getVehicleNumber());
                        startTracking.setText("Stop Journey");
                        UserDataWrapper.getInstance().serviceStarted(true);
                        ParticleSystem ps = new ParticleSystem(HomeActivity.this, 75, R.drawable.start_white, 1000);
                        ps.setScaleRange(0.7f, 1.3f);
                        ps.setSpeedRange(0.1f, 0.20f);
                        ps.setAcceleration(0.0001f, 90);
                        ps.setRotationSpeedRange(90, 180);
                        ps.setFadeOut(1000, new AccelerateInterpolator());
                        ps.oneShot(startTracking, 100);
                    } else {
                        startService.stopAllServices();
                        startTracking.setText("Start Journey");
                        UserDataWrapper.getInstance().serviceStarted(false);
                    }
                }
            });
            if(UserDataWrapper.getInstance().isFirstLaunch()){
                if (!UserDataWrapper.getInstance().IsServiceStarted()) {
                    startService.initializeSDK("whiteroads", "6ca383af9aa7d7c6538e746927ddc829",
                            UserDataWrapper.getInstance().getUserEmail(),
                            UserDataWrapper.getInstance().getVehicleNumber());
                    startTracking.setText("Stop Journey");
                    UserDataWrapper.getInstance().serviceStarted(true);
                    ParticleSystem ps = new ParticleSystem(HomeActivity.this, 75, R.drawable.start_white, 1000);
                    ps.setScaleRange(0.7f, 1.3f);
                    ps.setSpeedRange(0.1f, 0.20f);
                    ps.setAcceleration(0.0001f, 90);
                    ps.setRotationSpeedRange(90, 180);
                    ps.setFadeOut(1000, new AccelerateInterpolator());
                    ps.oneShot(startTracking, 100);
                } else {
                    startService.stopAllServices();
                    startTracking.setText("Start Journey");
                    UserDataWrapper.getInstance().serviceStarted(false);
                }
                UserDataWrapper.getInstance().setIsFirstLaunch();
            }
            transaction = getSupportFragmentManager().beginTransaction();
            HandleBottomNavigation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeToTrip(){
        try{
            navigation.setSelectedItemId(R.id.trips);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateData() {
        if (analysisFragment != null)
            analysisFragment.updateData();
    }

    private void HandleBottomNavigation() {
        try {
            analysisFragment = new AnalysisFragment();
            sensorsFragment = new SensorsFragment();
            active = analysisFragment;
            getSupportFragmentManager().beginTransaction().add(R.id.frame_container, sensorsFragment, "2").hide(sensorsFragment).commit();
            getSupportFragmentManager().beginTransaction().add(R.id.frame_container, analysisFragment, "1").commit();
            navigation = (BottomNavigationView) findViewById(R.id.navigationView);
            navigation.setVisibility(View.VISIBLE);
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            navigation.setSelectedItemId(R.id.home);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    if (!(active instanceof AnalysisFragment)) {
                        getSupportFragmentManager().beginTransaction().hide(active).show(analysisFragment).setCustomAnimations(R.animator.fade_in, R.animator.fade_out).commit();
                        active = analysisFragment;
                    }
                    return true;
                case R.id.trips:
                    if (!(active instanceof SensorsFragment)) {
                        getSupportFragmentManager().beginTransaction().hide(active).show(sensorsFragment).setCustomAnimations(R.animator.fade_in, R.animator.fade_out).commit();
                        active = sensorsFragment;
                    }
                    return true;
            }
            return false;
        }
    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        transaction.replace(R.id.frame_container, fragment);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
