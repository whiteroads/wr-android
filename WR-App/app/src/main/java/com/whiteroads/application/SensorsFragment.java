package com.whiteroads.application;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;
import com.whiteroads.application.data.UserDataWrapper;
import com.whiteroads.application.model.TripItem;
import com.whiteroads.application.model.TripSingleEntry;
import com.whiteroads.application.model.TripsData;
import com.whiteroads.application.networks.NetworksCalls;
import com.whiteroads.application.utils.CommonMethods;
import com.whiteroads.library.constants.EventBusContext;
import com.whiteroads.library.constants.NetworkConstants;

import java.util.Collections;

import de.greenrobot.event.EventBus;

public class SensorsFragment extends Fragment {
    private LinearLayout dynamicLyt;
    private TripsData tripsData;
    private EventBus eventBus;
    private LinearLayout noDataLayout, progressLayout;
    private Button refresh;
    private AVLoadingIndicatorView progress;
    private final static int SORT_DATA = 101;
    private RecyclerView recyclerView;
    private TripsAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for getActivity() fragment
        View view = inflater.inflate(R.layout.fragment_sensors, container, false);
        try {
            noDataLayout = (LinearLayout) view.findViewById(R.id.noDataLayout);
            progressLayout = (LinearLayout) view.findViewById(R.id.progressLayout);
            refresh = (Button) view.findViewById(R.id.refreshButton);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            progress = (AVLoadingIndicatorView) view.findViewById(R.id.progress);
            refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showHideLoading(true);
                    new LoadData().start();
                }
            });
            eventBus = new EventBus();
            eventBus.register(this);
            showHideLoading(true);
            new LoadData().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void showHideLoading(boolean value) {
        try {
            if (value) {
                progressLayout.setVisibility(View.VISIBLE);
                progress.show();
                noDataLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            } else {
                progressLayout.setVisibility(View.GONE);
                progress.hide();
                if (tripsData == null || tripsData.getData() == null || tripsData.getData().size() == 0) {
                    noDataLayout.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                } else {
                    noDataLayout.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void drawTrips() {
        try {
            adapter = new TripsAdapter(getContext(),tripsData);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(adapter);
            showHideLoading(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public class LoadData extends Thread {

        int requestType;

        public LoadData(int requestType) {
            this.requestType = requestType;
        }

        public LoadData() {

        }

        public void run() {
            super.run();
            try {
                if (requestType == SORT_DATA) {
                    for (TripItem item : tripsData.getData()) {
                        Collections.sort(item.getEntries(), TripSingleEntry.sortByPingTime());
                    }
                    calculatePhoneUsesMinutes();
                    calculateData();
                    calculateOverScore();
                    eventBus.post(new EventBusContext(SORT_DATA));
                    return;
                }
                if (CommonMethods.IsConnected(getActivity())) {
                    new NetworksCalls(getActivity()).getTrips(UserDataWrapper.getInstance().getUserEmail(), eventBus);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onEventMainThread(EventBusContext eventBusContext) {
        try {
            if (eventBusContext.getRequestCode() == SORT_DATA) {
                if (tripsData != null && tripsData.getData() != null && tripsData.getData().size() > 0) {
                    UserDataWrapper.getInstance().setTotalTrip(tripsData.getTrips());
                    UserDataWrapper.getInstance().setTotalDistance((float) tripsData.getDistance() / 1000);
                    ((HomeActivity) getActivity()).updateData();
                    drawTrips();
                }
                return;
            }
            if (eventBusContext.getResultCode() == NetworkConstants.ResultCodeSuccess) {
                tripsData = new Gson().fromJson(eventBusContext.getResponse(), TripsData.class);
                new LoadData(SORT_DATA).start();
            }
            if (eventBusContext.getRequestCode() == com.whiteroads.application.constants.NetworkConstants.ResultCodeFail) {
                showHideLoading(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculatePhoneUsesMinutes() {
        try {
            int i = 0;
            for (TripItem item : tripsData.getData()) {
                long time = 0;
                String firstTime = "";
                for (TripSingleEntry entry : item.getEntries()) {
                    if (!entry.getCallState().equalsIgnoreCase("idle")
                            && !entry.getCallState().equalsIgnoreCase("nan")) {
                        if (!firstTime.equalsIgnoreCase("")) {
                            time += CommonMethods.GetTwoDatesTimeDifference(firstTime,entry.getPingTime());
                        }
                        firstTime = entry.getPingTime();
                    } else {
                        firstTime = "";
                    }
                }
                tripsData.getData().get(i).setPhoneUsesMinutes(CommonMethods.round(time/60) + " Mins");
                i++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculateData() {
        try {
            double roadDamage = 0, finalScore = 0;

            //Calculate Road Damage
            int i = 0;
            for (TripItem item : tripsData.getData()) {
                double tempRoads = 0, overspeed = 0, accel = 0, hardbreaking = 0, phoneUse = 0;
                tempRoads = item.getRoadsDamageCount();
                overspeed = item.getOverspeedCount();
                accel = item.getRapidAccelaration();
                hardbreaking = item.getHardbreaking();
                phoneUse = item.getPhoneUses();
                roadDamage = CommonMethods.round((tempRoads / item.getEntries().size()) * 100);
                //Calculate Final Score
                double osScore = 0, accScore = 0, breakScore = 0, phoneScore = 0;
                osScore = CommonMethods.round((overspeed / item.getEntries().size()) * 100);
                accScore = CommonMethods.round((accel / item.getEntries().size()) * 100);
                breakScore = CommonMethods.round((hardbreaking / item.getEntries().size()) * 100);
                phoneScore = CommonMethods.round((phoneUse / item.getEntries().size()) * 100);
                finalScore = CommonMethods.round((1 / (osScore + 1) + 1 / (accScore + 1) + 1 / (breakScore + 1) + 1 / (phoneScore + 1)) * (100 / 4));

                tripsData.getData().get(i).setRoadDamage(roadDamage);
                if (finalScore > 0 && finalScore <= 25) {
                    tripsData.getData().get(i).setRating("Poor");
                } else if (finalScore > 25 && finalScore <= 50) {
                    tripsData.getData().get(i).setRating("Bad");
                } else if (finalScore > 50 && finalScore <= 75) {
                    tripsData.getData().get(i).setRating("Good");
                } else if (finalScore > 75 && finalScore <= 100) {
                    tripsData.getData().get(i).setRating("Best");
                }
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void calculateOverScore() {
        try {
            double roadDamage = 0, finalScore = 0;

            //Calculate Road Damage
            double overspeed = 0, accel = 0, hardbreaking = 0, phoneUse = 0;
            for (TripItem item : tripsData.getData()) {

                overspeed += item.getOverspeedCount();
                accel += item.getRapidAccelaration();
                hardbreaking += item.getHardbreaking();
                phoneUse += item.getPhoneUses();
            }
            //Calculate Final Score
            double osScore = 0, accScore = 0, breakScore = 0, phoneScore = 0;
            osScore = CommonMethods.round((overspeed / tripsData.getEntryCount()) * 100);
            accScore = CommonMethods.round((accel / tripsData.getEntryCount()) * 100);
            breakScore = CommonMethods.round((hardbreaking / tripsData.getEntryCount()) * 100);
            phoneScore = CommonMethods.round((phoneUse / tripsData.getEntryCount()) * 100);
            finalScore = CommonMethods.round((1 / (osScore + 1) + 1 / (accScore + 1) + 1 / (breakScore + 1) + 1 / (phoneScore + 1)) * (100 / 4));

            if (finalScore > 0 && finalScore <= 25) {
                UserDataWrapper.getInstance().setRating("Poor");
            } else if (finalScore > 25 && finalScore <= 50) {
                UserDataWrapper.getInstance().setRating("Bad");
            } else if (finalScore > 50 && finalScore <= 75) {
                UserDataWrapper.getInstance().setRating("Good");
            } else if (finalScore > 75 && finalScore <= 100) {
                UserDataWrapper.getInstance().setRating("Best");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
