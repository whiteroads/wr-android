package com.whiteroads.application;

import com.whiteroads.application.model.TripItem;

public class TripSingleton {

    private static TripSingleton object=null;
    private TripItem tripItem;
    private double roadDamage;
    private String rating;

    public double getRoadDamage() {
        return roadDamage;
    }

    public void setRoadDamage(double roadDamage) {
        this.roadDamage = roadDamage;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }


    private TripSingleton(){

    }

    public static TripSingleton getObject() {
        if(object == null){
            object = new TripSingleton();
        }
        return object;
    }

    public static void setObject(TripSingleton object) {
        TripSingleton.object = object;
    }

    public TripItem getTripItem() {
        return tripItem;
    }

    public void setTripItem(TripItem tripItem) {
        this.tripItem = tripItem;
    }
}
