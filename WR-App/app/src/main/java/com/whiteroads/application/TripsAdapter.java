package com.whiteroads.application;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.whiteroads.application.model.TripItem;
import com.whiteroads.application.model.TripsData;
import com.whiteroads.application.utils.CommonMethods;

public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.MyViewHolder> {

    private TripsData tripsData;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView trip, tripLocation, time,overspeed,hardbreak,phoneuse,rapid,score,roadDmaage;
        public LinearLayout tripLyt;

        public MyViewHolder(View view) {
            super(view);
            tripLyt = (LinearLayout) view.findViewById(R.id.tripLyt);
            trip = (TextView) view.findViewById(R.id.trip);
            tripLocation = (TextView) view.findViewById(R.id.tripLocation);
            time = (TextView) view.findViewById(R.id.time);
            overspeed = (TextView) view.findViewById(R.id.overspeed);
            hardbreak = (TextView) view.findViewById(R.id.hardbreak);
            phoneuse = (TextView) view.findViewById(R.id.phoneuse);
            rapid = (TextView) view.findViewById(R.id.rapid);
            score = (TextView) view.findViewById(R.id.score);
            roadDmaage = (TextView) view.findViewById(R.id.roadDmaage);
        }
    }


    public TripsAdapter(Context context,TripsData tripsData) {
        this.tripsData = tripsData;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.trip_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TripItem item = tripsData.getData().get(position);
        holder.score.setText(item.getRating());
        holder.time.setText(CommonMethods.convertTimeFromOneFormatToAnother(item.getEntries().get(0).getPingTime(), "yyyy-MM-dd HH:mm:ss",
                "dd MMM yyyy - HH:mm a") + " To " + CommonMethods.convertTimeFromOneFormatToAnother(item.getEntries().get(item.getEntries().size() - 1).getPingTime(), "yyyy-MM-dd HH:mm:ss",
                "HH:mm a"));
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String location = CommonMethods.getAddress(context, Double.parseDouble(item.getEntries().get(0).getLat()), Double.parseDouble(item.getEntries().get(0).getLng())) + " To "
                            + CommonMethods.getAddress(context, Double.parseDouble(item.getEntries().get(item.getEntries().size() - 1).getLat()), Double.parseDouble(item.getEntries().get(item.getEntries().size() - 1).getLng()));

                    ((Activity)context).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            holder.tripLocation.setText(location);
                        }
                        //job done on the UI thread
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        holder.overspeed.setText(item.getOverspeedCount() + "");
        holder.trip.setText("Trip Number: " + item.getTripId());
        holder.phoneuse.setText(item.getPhoneUsesMinutes());
        holder.hardbreak.setText(item.getHardbreaking() + "");
        holder.rapid.setText(item.getRapidAccelaration() + "");
        holder.roadDmaage.setText(CommonMethods.round(item.getRoadDamage()) + "%");

        holder.tripLyt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TripSingleton.getObject().setTripItem(item);
                Intent intent = new Intent(context, DetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tripsData.getData().size();
    }
}
