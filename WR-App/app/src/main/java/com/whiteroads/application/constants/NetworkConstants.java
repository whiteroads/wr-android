package com.whiteroads.application.constants;

public class NetworkConstants {

    public static final String TAG = "ExceptionLog";
    public static final String BASE_URL = "http://api.whiteroads.ai:8585/";
    public static final String LOGIN_USER = "user/login.ns";

    public final static String loginData = "loginData";
    public static final String UserId = "user_id";
    public static final String Email = "email";
    public static final String TRIPS_LIST = "user/trips_data.ns";



    //common response params
    public final static int ResultCodeFail = 0;
    public final static int ResultCodeSuccess = 1;
}
