package com.whiteroads.application.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.whiteroads.application.DAApplication;

public class UserDataWrapper {


    public static final String DATABASE_NAME = "app_user_db";
    private static final String USER_ACCOUNT = "app_user_account";
    private static final String USER_ACCOUNT_EMAIl = "app_user_account_email";
    private static final String VEHICLE_NUMBER = "app_vehicle_number";
    private static final String MOBILE_NUMBER = "app_mobile_number";
    private static final String USER_NAME = "app_user_name";
    private static final String USER_PROFILE_PIC = "app_user_profile_pic";
    private static final String IS_SERVICE_STARTED = "app_is_service_started";
    private static final String TOTAL_TRIPS = "app_total_trips";
    private static final String TOTAL_DISTANCE = "app_total_distance";
    private static final String APP_RATING = "app_rating";
    private static final String IS_FIRST_LAUNCH = "is_first_launch";


    private static UserDataWrapper sInstance;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private UserDataWrapper() {
        mPreferences = (DAApplication.getCustomAppContext()).getSharedPreferences(DATABASE_NAME, Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }

    public static UserDataWrapper getInstance() {
        if (sInstance == null) {
            sInstance = new UserDataWrapper();
        }
        return sInstance;
    }

    public SharedPreferences getPref() {
        return mPreferences;
    }

    public void saveUserId(int id) {
        try {
            mEditor.putInt(USER_ACCOUNT, id).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getUserId() {
        try {
            return mPreferences.getInt(USER_ACCOUNT, -1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void saveVehicleNumber(String number) {
        try {
            mEditor.putString(VEHICLE_NUMBER, number).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getVehicleNumber() {
        try {
            return mPreferences.getString(VEHICLE_NUMBER, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveUserEmail(String email) {
        try {
            mEditor.putString(USER_ACCOUNT_EMAIl, email).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserEmail() {
        try {
            return mPreferences.getString(USER_ACCOUNT_EMAIl, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveUserMobile(String mobile) {
        try {
            mEditor.putString(MOBILE_NUMBER,  mobile).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserMobile() {
        try {
            return mPreferences.getString(MOBILE_NUMBER, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveUserName(String name) {
        try {
            mEditor.putString(USER_NAME,  name).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserName() {
        try {
            return mPreferences.getString(USER_NAME, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void saveUserPic(String name) {
        try {
            mEditor.putString(USER_PROFILE_PIC,  name).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUserPic() {
        try {
            return mPreferences.getString(USER_PROFILE_PIC, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
    public void serviceStarted(boolean value) {
        try {
            mEditor.putBoolean(IS_SERVICE_STARTED,  value).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean IsServiceStarted() {
        try {
            return mPreferences.getBoolean(IS_SERVICE_STARTED, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setTotalTrip(int value) {
        try {
            mEditor.putInt(TOTAL_TRIPS,  value).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getTotalTrips() {
        try {
            return mPreferences.getInt(TOTAL_TRIPS, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setTotalDistance(float value) {
        try {
            mEditor.putFloat(TOTAL_DISTANCE,  value).apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public float getTotalDistance() {
        try {
            return mPreferences.getFloat(TOTAL_DISTANCE, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setRating(String value) {
        try {
            mEditor.putString(APP_RATING,  value).apply();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRating() {
        try {
            return mPreferences.getString(APP_RATING, "Good");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Good";
    }

    public void setIsFirstLaunch() {
        try {
            mEditor.putBoolean(IS_FIRST_LAUNCH,  false).apply();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isFirstLaunch() {
        try {
            return mPreferences.getBoolean(IS_FIRST_LAUNCH, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
