package com.whiteroads.application.model;

import java.util.List;

public class TripItem {
    private int tripId;
    private List<TripSingleEntry> entries;
    private double distance;
    private int overspeedCount;
    private int phoneUses;
    private int rapidAccelaration;
    private int hardbreaking;
    private int roadsDamageCount;
    private double roadDamage;
    private String rating;

    private String phoneUsesMinutes;

    public String getPhoneUsesMinutes() {
        return phoneUsesMinutes;
    }

    public void setPhoneUsesMinutes(String phoneUsesMinutes) {
        this.phoneUsesMinutes = phoneUsesMinutes;
    }

    public double getRoadDamage() {
        return roadDamage;
    }

    public void setRoadDamage(double roadDamage) {
        this.roadDamage = roadDamage;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getRoadsDamageCount() {
        return roadsDamageCount;
    }

    public void setRoadsDamageCount(int roadsDamageCount) {
        this.roadsDamageCount = roadsDamageCount;
    }

    public int getOverspeedCount() {
        return overspeedCount;
    }

    public void setOverspeedCount(int overspeedCount) {
        this.overspeedCount = overspeedCount;
    }

    public int getPhoneUses() {
        return phoneUses;
    }

    public void setPhoneUses(int phoneUses) {
        this.phoneUses = phoneUses;
    }

    public int getRapidAccelaration() {
        return rapidAccelaration;
    }

    public void setRapidAccelaration(int rapidAccelaration) {
        this.rapidAccelaration = rapidAccelaration;
    }

    public int getHardbreaking() {
        return hardbreaking;
    }

    public void setHardbreaking(int hardbreaking) {
        this.hardbreaking = hardbreaking;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public List<TripSingleEntry> getEntries() {
        return entries;
    }

    public void setEntries(List<TripSingleEntry> entries) {
        this.entries = entries;
    }
}
