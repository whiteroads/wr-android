package com.whiteroads.application.model;

import com.whiteroads.application.utils.CommonMethods;

import java.util.Comparator;

public class TripSingleEntry{
    private String lat;
    private String lng;
    private double speed;
    private String callState;
    private String pingTime;
    private int overspeed;
    private int acceleration;
    private int roadDamage;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getCallState() {
        return callState;
    }

    public void setCallState(String callState) {
        this.callState = callState;
    }

    public String getPingTime() {
        return pingTime;
    }

    public void setPingTime(String pingTime) {
        this.pingTime = pingTime;
    }

    public int getOverspeed() {
        return overspeed;
    }

    public void setOverspeed(int overspeed) {
        this.overspeed = overspeed;
    }

    public int getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(int acceleration) {
        this.acceleration = acceleration;
    }

    public int getRoadDamage() {
        return roadDamage;
    }

    public void setRoadDamage(int roadDamage) {
        this.roadDamage = roadDamage;
    }

    public static Comparator<TripSingleEntry> sortByPingTime() throws Exception {
        Comparator<TripSingleEntry> comp = new Comparator<TripSingleEntry>() {
            @Override
            public int compare(TripSingleEntry r1, TripSingleEntry r2) {
                try{
                    if (CommonMethods.getMilliSecondsFromDateTime(r1.getPingTime())>CommonMethods.getMilliSecondsFromDateTime(r2.getPingTime())) {
                        return 1;
                    }else if(CommonMethods.getMilliSecondsFromDateTime(r1.getPingTime())==CommonMethods.getMilliSecondsFromDateTime(r2.getPingTime())){
                        return 0;
                    }else{
                        return -1;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
                return 0;
            }
        };
        return comp;
    }
}

