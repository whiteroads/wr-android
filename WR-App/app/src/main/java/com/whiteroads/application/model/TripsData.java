package com.whiteroads.application.model;
import java.util.ArrayList;

public class TripsData extends CommonResponse{
    private ArrayList<TripItem> data;
    private int trips;
    private double distance;
    private int entryCount;

    public int getEntryCount() {
        return entryCount;
    }

    public void setEntryCount(int entryCount) {
        this.entryCount = entryCount;
    }

    public int getTrips() {
        return trips;
    }

    public void setTrips(int trips) {
        this.trips = trips;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public ArrayList<TripItem> getData() {
        return data;
    }

    public void setData(ArrayList<TripItem> data) {
        this.data = data;
    }
}