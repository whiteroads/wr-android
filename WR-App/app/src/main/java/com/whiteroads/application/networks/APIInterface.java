package com.whiteroads.application.networks;

import com.whiteroads.application.constants.NetworkConstants;
import com.whiteroads.application.model.TripsData;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {
    @FormUrlEncoded
    @POST
    Call<TripsData> getTrips(
            @Url String url,
            @Field(NetworkConstants.Email) String email);


}
