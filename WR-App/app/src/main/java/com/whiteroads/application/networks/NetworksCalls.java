package com.whiteroads.application.networks;

import android.content.Context;

import com.google.gson.Gson;
import com.whiteroads.application.constants.NetworkConstants;
import com.whiteroads.application.model.TripsData;
import com.whiteroads.library.constants.EventBusContext;
import com.whiteroads.library.networks.RetrofitClientInstance;

import de.greenrobot.event.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworksCalls {
    private Context context;

    public NetworksCalls(Context context) {
        this.context = context;
    }

    public void getTrips(String email, final EventBus eventBus) {
        try {
            //email = "sureshmehto1981@gmail.com";
            String url = NetworkConstants.BASE_URL + NetworkConstants.TRIPS_LIST;
            APIInterface service = RetrofitClientInstance.getRetrofitInstance().create(APIInterface.class);
            Call<TripsData> call = service.getTrips(url,email);
            call.enqueue(new Callback<TripsData>() {
                @Override
                public void onResponse(Call<TripsData> call, Response<TripsData> response) {
                    TripsData tripsData = (TripsData) response.body();
                    if(tripsData!=null && tripsData.getResultCode()==NetworkConstants.ResultCodeSuccess){
                        eventBus.post(new EventBusContext(new Gson().toJson(tripsData),tripsData.getResultCode()));
                    }else{
                        eventBus.post(new EventBusContext(NetworkConstants.ResultCodeFail));
                    }

                }

                @Override
                public void onFailure(Call<TripsData> call, Throwable t) {
                    eventBus.post(new EventBusContext(NetworkConstants.ResultCodeFail));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
