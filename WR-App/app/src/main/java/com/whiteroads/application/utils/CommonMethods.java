package com.whiteroads.application.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class CommonMethods {

    public static void ShowToast(Context context,
                                 String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean IsConnected(Context context) {

        try
        {
            final ConnectivityManager conMgr =  (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.getState() == NetworkInfo.State.CONNECTED;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return true;
    }

    public static double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        try {
            bd = bd.setScale(2, RoundingMode.HALF_UP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bd.doubleValue();
    }

    public static String convertTimeFromOneFormatToAnother(String time, String existingFormat, String desiredFormat) {
        SimpleDateFormat sdfyy = new SimpleDateFormat(existingFormat, Locale.ENGLISH);
        SimpleDateFormat sdfdd = new SimpleDateFormat(desiredFormat, Locale.ENGLISH);
        if (time != null && time != "") {
            try {
                sdfyy.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                sdfdd.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                Date d = sdfyy.parse(time);
                String dIndd = sdfdd.format(d);
                return dIndd;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";

    }
    public static long getMilliSecondsFromDateTime(String dateTime) {
        try {
            if (dateTime != null && !dateTime.isEmpty() && !dateTime.equalsIgnoreCase("")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                return sdf.parse(dateTime).getTime();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getAddress(Context context, double lat, double lng) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            if(obj.getSubLocality()==null){
                return obj.getAdminArea();
            }else{
                return obj.getSubLocality();
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "";
    }

    public static long GetTwoDatesTimeDifferenceMinutes(String latestDate, String oldDate) {

        if (latestDate != null && !latestDate.equals("") && oldDate != null && !oldDate.equals("")) {
            SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            d.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            long difference = 0;
            try {
                Date current = d.parse(latestDate);
                Date last = d.parse(oldDate);
                difference = -(current.getTime() - last.getTime());
                difference = difference / (60 * 1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return difference;
        }
        return 0;
    }
    public static long GetTwoDatesTimeDifference(String latestDate, String oldDate) {

        if (latestDate != null && !latestDate.equals("") && oldDate != null && !oldDate.equals("")) {
            SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            d.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            long difference = 0;
            try {
                Date current = d.parse(latestDate);
                Date last = d.parse(oldDate);
                difference = -(current.getTime() - last.getTime());
                difference = difference / (1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return difference;
        }
        return 0;
    }
    public static String getAddressComplete(Context context,Location location) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(context, Locale.getDefault());
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            return addresses.get(0).getAddressLine(0) + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
